ARG BUILD_ARCH=x64

#FROM forumi0721alpine${BUILD_ARCH}build/alpine-${BUILD_ARCH}-jellyfin-builder-ffmpeg:latest as ffmpeg

#FROM forumi0721alpine${BUILD_ARCH}build/alpine-${BUILD_ARCH}-jellyfin-builder-libskiasharp:latest as libskiasharp

FROM forumi0721alpinex64/alpine-x64-dotnet-sdk:latest as jellyfin

LABEL maintainer="forumi0721@gmail.com"

#COPY --from=ffmpeg /output /app

#COPY --from=libskiasharp /output /app

COPY local/. /usr/local/

#RUN ["docker-build-start"]

RUN ["docker-init"]

#RUN ["docker-build-end"]



FROM forumi0721/busybox-${BUILD_ARCH}-base:latest

LABEL maintainer="forumi0721@gmail.com"

COPY --from=jellyfin /output /output

